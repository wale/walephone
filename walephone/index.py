import urllib.parse
from fastapi import FastAPI, Response, Request
from twilio.twiml.voice_response import VoiceResponse
from io import BytesIO
from dhooks import Webhook, File

from .config import config

import toml, uvicorn, urllib, requests

app = FastAPI()

@app.post("/record")
async def record():
    """ Returns a TwiML response, and records a message. """

    # Start our TwiML response
    response = VoiceResponse()
    # Use <Say> to give the caller some instructions
    response.say(config["twilio"]["message"])
    # Use <Record> to record the caller's message
    response.record(action="/recorded")
    # End the call with <Hangup>
    response.hangup()

    return Response(content=str(response), media_type="text/xml")

@app.post("/recorded")
async def recorded(request: Request):
    """ Submits a Discord webhook. """

    # Parse parameters from the body querystring.
    raw_params = await request.body()
    params = urllib.parse.parse_qs(raw_params.decode())
    
    url = config["discord"]["webhook"]
    hook = Webhook(url)

    # Download recording to attach to the webhook.
    recording = requests.get(params["RecordingUrl"][0])
    recording_file = File(BytesIO(recording.content), name="recording.wav")

    hook.send("New call from :flag_{country}:!".format(country = params["CallerCountry"][0].lower()), file=recording_file)

    return {"message": "Successfully sent webhook."}

def run():
    uvicorn.run("walephone.index:app", port=config["http"]["port"], host="0.0.0.0", log_level="info")

